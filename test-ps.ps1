param(
    [string[]]$Tasks
    )

function Run-Build
{
    mvn -s .m2/settings.xml --batch-mode compile
}

function Run-Test
{
    mvn -s .m2/settings.xml --batch-mode test
}

function Run-Deploy
{
    mvn -s .m2/settings.xml --batch-mode deploy
}

foreach($task in $Tasks){
    switch($task)
    {
        "deploy" {
            Run-Deploy
        }
    }
}